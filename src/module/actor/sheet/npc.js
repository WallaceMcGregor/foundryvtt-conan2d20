import ActorSheetConan2d20 from './base';

class ActorSheetConan2d20NPC extends ActorSheetConan2d20 {
  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      classes: options.classes.concat(['conan2d20', 'actor', 'npc-sheet']),
      width: 460,
      height: 680,
      resizable: false,
      scrollY: ['.sheet-content'],
    });
    return options;
  }

  get template() {
    const path = 'systems/conan2d20/templates/actors/';
    if (!game.user.isGM && this.actor.limited)
      return `${path}readonly-npc-sheet.html`;
    return `${path}npc-sheet.html`;
  }

  async getData(options = {}) {
    const context = await super.getData(options);
    context.flags = context.actor.flags;

    // Update expertise fields labels
    if (context.system.skills !== undefined) {
      for (const [s, skl] of Object.entries(context.system.skills)) {
        skl.label = CONFIG.CONAN.expertiseFields[s];
      }
    }

    // Remove dead fields if they exist
    delete context.system.isMinion;
    delete context.system.isNemesis;
    delete context.system.isToughened;

    context.npcTypes = CONFIG.CONAN.npcTypes;
    context.npcTraits = CONFIG.CONAN.npcTraits;

    context.skills = CONFIG.CONAN.expertiseFields;

    return context;
  }

  _prepareItems(context) {
    const attacks = {
      npcattack: {label: 'NPC Attack', items: []},
    };

    const actions = {
      abilities: {
        label: game.i18n.localize('CONAN.npcActionTypes.abilities'),
        actions: [],
      },
      doom: {
        label: game.i18n.localize('CONAN.npcActionTypes.doom'),
        actions: [],
      },
    };

    // Get Attacks
    for (const i of context.items) {
      i.img = i.img || CONST.DEFAULT_TOKEN;

      if (Object.keys(attacks).includes(i.type)) {
        if (i.type === 'npcattack') {
          let item;
          try {
            item = this.actor.getEmbeddedDocument('Item', i._id);
            i.chatData = item.getChatData({secrets: this.actor.isOwner});
          } catch (err) {
            console.error(
              `Conan 2D20 System | NPC Sheet | Could not load item ${i.name}`
            );
          }
          attacks[i.type].items.push(i);
        }
      } else if (i.type === 'npcaction') {
        const actionType = i.system.actionType || 'npcaction';
        actions[actionType].actions.push(i);
      }

      if (i.type !== 'npcattack' && i.type !== 'npcaction') {
        // Invalid Items
        console.log('Invalid item for non-player characters!');
        this.actor.deleteEmbeddedDocuments('Item', [i.id]);
      }
    }

    context.actions = actions;
    context.attacks = attacks;
  }
}

export default ActorSheetConan2d20NPC;
