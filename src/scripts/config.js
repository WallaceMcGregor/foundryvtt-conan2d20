/* eslint-disable no-shadow */

export const CONFIG = {};

CONFIG.attributes = {
  bra: 'CONAN.attributes.bra',
  agi: 'CONAN.attributes.agi',
  awa: 'CONAN.attributes.awa',
  coo: 'CONAN.attributes.coo',
  int: 'CONAN.attributes.int',
  wil: 'CONAN.attributes.wil',
  per: 'CONAN.attributes.per',
};

CONFIG.attributeTitles = {
  bra: 'CONAN.attributeTitles.bra',
  agi: 'CONAN.attributeTitles.agi',
  awa: 'CONAN.attributeTitles.awa',
  coo: 'CONAN.attributeTitles.coo',
  int: 'CONAN.attributeTitles.int',
  wil: 'CONAN.attributeTitles.wil',
  per: 'CONAN.attributeTitles.per',
};

CONFIG.ASSIST_2D20_DICE = 1;
CONFIG.BASE_2D20_DICE = 2;
CONFIG.MAX_2D20_DICE = 5;
CONFIG.MAX_2D20_PURCHASE = 3;

CONFIG.skills = {
  acr: 'CONAN.skills.acr',
  mel: 'CONAN.skills.mel',
  ste: 'CONAN.skills.ste',
  ins: 'CONAN.skills.ins',
  obs: 'CONAN.skills.obs',
  sur: 'CONAN.skills.sur',
  thi: 'CONAN.skills.thi',
  ath: 'CONAN.skills.ath',
  res: 'CONAN.skills.res',
  par: 'CONAN.skills.par',
  ran: 'CONAN.skills.ran',
  sai: 'CONAN.skills.sai',
  alc: 'CONAN.skills.alc',
  cra: 'CONAN.skills.cra',
  hea: 'CONAN.skills.hea',
  lin: 'CONAN.skills.lin',
  lor: 'CONAN.skills.lor',
  war: 'CONAN.skills.war',
  ani: 'CONAN.skills.ani',
  com: 'CONAN.skills.com',
  cou: 'CONAN.skills.cou',
  per: 'CONAN.skills.per',
  soc: 'CONAN.skills.soc',
  dis: 'CONAN.skills.dis',
  sor: 'CONAN.skills.sor',
};

CONFIG.skillAttributeMap = {
  acr: 'agi',
  mel: 'agi',
  ste: 'agi',
  ins: 'awa',
  obs: 'awa',
  sur: 'awa',
  thi: 'awa',
  ath: 'bra',
  res: 'bra',
  par: 'coo',
  ran: 'coo',
  sai: 'coo',
  alc: 'int',
  cra: 'int',
  hea: 'int',
  lin: 'int',
  lor: 'int',
  war: 'int',
  ani: 'per',
  com: 'per',
  cou: 'per',
  per: 'per',
  soc: 'per',
  dis: 'wil',
  sor: 'wil',
};

CONFIG.enchantmentExplodingItems = {
  flashPaper: 'CONAN.enchantmentExplodingItems.fla',
  smallFireworks: 'CONAN.enchantmentExplodingItems.sma',
  loudFireworks: 'CONAN.enchantmentExplodingItems.lou',
  largeFireworks: 'CONAN.enchantmentExplodingItems.lar',
  smallExplosives: 'CONAN.enchantmentExplodingItems.sme',
  largeExplosives: 'CONAN.enchantmentExplodingItems.lex',
};

CONFIG.enchantmentStrengths = {
  weak: 'CONAN.enchantmentStrengths.wea',
  average: 'CONAN.enchantmentStrengths.ave',
  potent: 'CONAN.enchantmentStrengths.pot',
  dangerous: 'CONAN.enchantmentStrengths.dan',
  extraordinary: 'CONAN.enchantmentStrengths.ext',
  devastationg: 'CONAN.enchantmentStrengths.dev',
};

CONFIG.enchantmentBlindingStrengths = {
  regular: 'CONAN.enchantmentBlindingStrengths.reg',
  dry: 'CONAN.enchantmentBlindingStrengths.dry',
  fine: 'CONAN.enchantmentBlindingStrengths.fin',
  perfumed: 'CONAN.enchantmentBlindingStrengths.per',
  burning: 'CONAN.enchantmentBlindingStrengths.bur',
};

CONFIG.enchantmentTalismanTypes = {
  hamsa: 'CONAN.enchantmentTalismanTypes.ham',
  chasme: 'CONAN.enchantmentTalismanTypes.cha',
  pictish: 'CONAN.enchantmentTalismanTypes.pic',
  nazar: 'CONAN.enchantmentTalismanTypes.naz',
  animal: 'CONAN.enchantmentTalismanTypes.ani',
};

CONFIG.enchantmentTypes = {
  explodingPowder: 'CONAN.enchantmentTypes.exp',
  blindingPowder: 'CONAN.enchantmentTypes.bli',
  burningLiquid: 'CONAN.enchantmentTypes.bur',
  reinforcedFabric: 'CONAN.enchantmentTypes.rei',
  upasGlass: 'CONAN.enchantmentTypes.upa',
  talisman: 'CONAN.enchantmentTypes.tal',
  lotusPollen: 'CONAN.enchantmentTypes.lot',
};

CONFIG.upasGlassSizes = {
  resilient: 'CONAN.upasGlassSizes.res',
  strengthened: 'CONAN.upasGlassSizes.str',
  unbreakable: 'CONAN.upasGlassSizes.unb',
};

CONFIG.enchantmentVolatilities = {
  burningAlcohol: 'CONAN.enchantmentVolatilities.bur',
  explodingLiquor: 'CONAN.enchantmentVolatilities.exp',
  volatileSpirits: 'CONAN.enchantmentVolatilities.vol',
  hellishBrimstone: 'CONAN.enchantmentVolatilities.hel',
};

CONFIG.expertiseFields = {
  mov: 'CONAN.expertiseFields.mov',
  cmb: 'CONAN.expertiseFields.cmb',
  frt: 'CONAN.expertiseFields.frt',
  knw: 'CONAN.expertiseFields.knw',
  scl: 'CONAN.expertiseFields.scl',
  sns: 'CONAN.expertiseFields.sns',
};

CONFIG.expertiseAttributeMap = {
  mov: 'agi',
  cmb: 'agi',
  frt: 'bra',
  knw: 'int',
  scl: 'per',
  sns: 'awa',
};

CONFIG.rollDifficultyLevels = {
  0: 'CONAN.skillRollDifficultyLevels.0',
  1: 'CONAN.skillRollDifficultyLevels.1',
  2: 'CONAN.skillRollDifficultyLevels.2',
  3: 'CONAN.skillRollDifficultyLevels.3',
  4: 'CONAN.skillRollDifficultyLevels.4',
  5: 'CONAN.skillRollDifficultyLevels.5',
};

CONFIG.skillRollResourceSpends = {
  momentum: 'CONAN.skillRollResourceSpends.mome',
  doom: 'CONAN.skillRollResourceSpends.doom',
};

CONFIG.rollResults = {
  success: 'CONAN.skillRollSuccess',
  failure: 'CONAN.skillRollFailure',
};

CONFIG.enchantmentIngredients = {
  0: '0',
  1: '1',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
  6: '6',
  7: '7',
  8: '8',
  9: '9',
};

CONFIG.attacks = {
  weapon: 'CONAN.attackTypes.weapon',
  display: 'CONAN.attackTypes.display',
};

CONFIG.attackTypes = {
  melee: 'CONAN.attackTypes.melee',
  ranged: 'CONAN.attackTypes.ranged',
  threaten: 'CONAN.attackTypes.threaten',
};

CONFIG.damageTypes = {
  mental: 'CONAN.damageTypes.mental',
  physical: 'CONAN.damageTypes.physical',
};

CONFIG.npcActionTypes = {
  abilities: 'CONAN.npcActionTypes.abilities',
  doom: 'CONAN.npcActionTypes.doom',
};

CONFIG.npcTraits = {
  horror: 'CONAN.npcCategories.horror',
  undead: 'CONAN.npcCategories.undead',
};

CONFIG.npcAttackTypes = {
  melee: 'CONAN.npcAttackTypes.melee',
  ranged: 'CONAN.npcAttackTypes.ranged',
  threaten: 'CONAN.npcAttackTypes.threaten',
};

CONFIG.npcTypes = {
  minion: 'CONAN.npcCategories.minion',
  toughened: 'CONAN.npcCategories.toughened',
  nemesis: 'CONAN.npcCategories.nemesis',
};

CONFIG.availabilityTypes = {
  1: 'CONAN.skillRollDifficultyLevels.1',
  2: 'CONAN.skillRollDifficultyLevels.2',
  3: 'CONAN.skillRollDifficultyLevels.3',
  4: 'CONAN.skillRollDifficultyLevels.4',
  5: 'CONAN.skillRollDifficultyLevels.5',
};

CONFIG.conditionTypes = {
  blind: 'CONAN.conditions.bli',
  burningx: 'CONAN.conditions.bur',
  dazed: 'CONAN.conditions.daz',
  deaf: 'CONAN.conditions.dea',
  guardBroken: 'CONAN.conditions.gua',
  hindered: 'CONAN.conditions.hin',
  poisoned: 'CONAN.conditions.poi',
  prone: 'CONAN.conditions.pro',
  staggered: 'CONAN.conditions.sta',
};

CONFIG.naturesTypes = {
  cautious: 'CONAN.natures.cautious',
  curious: 'CONAN.natures.curious',
  inspirational: 'CONAN.natures.inspirational',
  learned: 'CONAN.natures.learned',
  practical: 'CONAN.natures.practical',
  scheming: 'CONAN.natures.scheming',
  sneaky: 'CONAN.natures.sneaky',
  stoic: 'CONAN.natures.stoic',
  supportive: 'CONAN.natures.supportive',
  wrathful: 'CONAN.natures.wrathful',
};

CONFIG.coverageTypes = {
  head: 'CONAN.coverage.head',
  torso: 'CONAN.coverage.torso',
  larm: 'CONAN.coverage.larm',
  rarm: 'CONAN.coverage.rarm',
  lleg: 'CONAN.coverage.lleg',
  rleg: 'CONAN.coverage.rleg',
};

CONFIG.armorTypes = {
  heavyCloth: 'CONAN.armorTypes.heavycloth',
  lightArmor: 'CONAN.armorTypes.lightarmor',
  heavyArmor: 'CONAN.armorTypes.heavyarmor',
  veryHeavyArmor: 'CONAN.armorTypes.vheavyarmor',
};

CONFIG.armorQualities = {
  brittle: 'CONAN.qualities.armor.brit',
  bulky: 'CONAN.qualities.armor.bulk',
  cool: 'CONAN.qualities.armor.cool',
  couragex: 'CONAN.qualities.armor.cour',
  fragile: 'CONAN.qualities.armor.frag',
  heavy: 'CONAN.qualities.armor.heav',
  intimidating: 'CONAN.qualities.armor.inti',
  noisy: 'CONAN.qualities.armor.nois',
  mentalPiercingx: 'CONAN.qualities.armor.apie',
  vheavy: 'CONAN.qualities.armor.very',
  warm: 'CONAN.qualities.armor.warm',
};

CONFIG.actionTypes = {
  passive: 'CONAN.actionPassive',
  free: 'CONAN.actionFree',
  minor: 'CONAN.actionMinor',
  standard: 'CONAN.actionStandard',
  reaction: 'CONAN.actionReaction',
};

CONFIG.freeActions = {
  adjust: 'CONAN.actions.free.adj',
  dropItem: 'CONAN.actions.free.dro',
  dropProne: 'CONAN.actions.free.pro',
  simpleTask: 'CONAN.actions.free.sim',
  speak: 'CONAN.actions.free.spe',
};

CONFIG.minorActions = {
  clear: 'CONAN.actions.minor.cle',
  drawItem: 'CONAN.actions.minor.dra',
  movement: 'CONAN.actions.minor.mov',
  regainGuard: 'CONAN.actions.minor.reg',
  stand: 'CONAN.actions.minor.sta',
};

CONFIG.standardActions = {
  assist: 'CONAN.actions.standard.ass',
  attack: 'CONAN.actions.standard.att',
  brace: 'CONAN.actions.standard.bra',
  exploit: 'CONAN.actions.standard.exp',
  pass: 'CONAN.actions.standard.pas',
  ready: 'CONAN.actions.standard.rea',
  recover: 'CONAN.actions.standard.rec',
  skillTest: 'CONAN.actions.standard.ski',
  sprint: 'CONAN.actions.standard.spr',
  treatment: 'CONAN.actions.standard.tre',
  withdraw: 'CONAN.actions.standard.wit',
};

CONFIG.reactionActions = {
  defend: 'CONAN.actions.reaction.def',
  protect: 'CONAN.actions.reaction.pro',
  retaliate: 'CONAN.actions.reaction.ret',
};

CONFIG.actionCategories = {
  defensive: 'CONAN.actionCategories.def',
  offensive: 'CONAN.actionCategories.off',
  interaction: 'CONAN.actionCategories.int',
  movement: 'CONAN.actionCategories.mov',
};

CONFIG.actionCounts = {
  1: 'CONAN.actionCounts.1',
  2: 'CONAN.actionCounts.2',
  '1r': 'CONAN.actionCounts.1r',
};

CONFIG.kitTypes = {
  facility: 'CONAN.kitTypes.fac',
  kit: 'CONAN.kitTypes.kit',
  library: 'CONAN.kitTypes.lib',
  reload: 'CONAN.kitTypes.rel',
  resource: 'CONAN.kitTypes.res',
  tool: 'CONAN.kitTypes.too',
};

CONFIG.kitUses = {
  1: '1',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
  6: '6',
  inf: '&infin;',
};

CONFIG.lotusPollenColors = {
  black: 'CONAN.lotusPollenColors.black',
  purple: 'CONAN.lotusPollenColors.purple',
  yellow: 'CONAN.lotusPollenColors.yellow',
  green: 'CONAN.lotusPollenColors.green',
  gray: 'CONAN.lotusPollenColors.gray',
  golden: 'CONAN.lotusPollenColors.golden',
};

CONFIG.lotusPollenDifficulty = {
  per: 'CONAN.lotusPollenDifficulty',
};

CONFIG.lotusPollenForms = {
  gas: 'CONAN.lotusPollenForms.gas',
  powder: 'CONAN.lotusPollenForms.pow',
  liquid: 'CONAN.lotusPollenForms.liq',
};

CONFIG.lotusPollenUses = {
  opiate: 'CONAN.lotusPollenUses.opi',
  poison: 'CONAN.lotusPollenUses.poi',
  paralytic: 'CONAN.lotusPollenUses.par',
  hallucinogenic: 'CONAN.lotusPollenUses.hal',
  enchantment: 'CONAN.lotusPollenUses.enc',
  anger: 'CONAN.lotusPollenUses.ang',
  madness: 'CONAN.lotusPollenUses.mad',
};

CONFIG.languages = {
  afghuli: 'CONAN.languages.afgh',
  argossean: 'CONAN.languages.argo',
  aquilonian: 'CONAN.languages.aqui',
  brythunian: 'CONAN.languages.bryt',
  corinthian: 'CONAN.languages.cori',
  cimmerian: 'CONAN.languages.cimm',
  darfari: 'CONAN.languages.darf',
  hyperborean: 'CONAN.languages.hype',
  hyrkanian: 'CONAN.languages.hyrk',
  iranistani: 'CONAN.languages.iran',
  keshani: 'CONAN.languages.kesh',
  kothic: 'CONAN.languages.koth',
  kushite: 'CONAN.languages.kush',
  nemedian: 'CONAN.languages.neme',
  nordheimer: 'CONAN.languages.nord',
  ophirian: 'CONAN.languages.ophi',
  punt: 'CONAN.languages.punt',
  shemitish: 'CONAN.languages.shem',
  stygian: 'CONAN.languages.styg',
  turanian: 'CONAN.languages.tura',
  vendhyan: 'CONAN.languages.vend',
  zamorian: 'CONAN.languages.zamo',
  zembabwein: 'CONAN.languages.zemb',
  zingaran: 'CONAN.languages.zing',
};

CONFIG.statusEffects = [
  {
    icon: 'systems/conan2d20/assets/icons/conditions/blind.png',
    id: 'blind',
    label: 'CONAN.conditions.bli',
    title: 'CONAN.conditionDescriptionBlin',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/burningx.png',
    id: 'burningx',
    label: 'CONAN.conditions.bur',
    title: 'CONAN.conditionDescriptionBurn',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: 1,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/dazed.png',
    id: 'dazed',
    label: 'CONAN.conditions.daz',
    title: 'CONAN.conditionDescriptionDaze',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/deaf.png',
    id: 'deaf',
    label: 'CONAN.conditions.dea',
    title: 'CONAN.conditionDescriptionDeaf',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/guardbreak.png',
    id: 'guardBroken',
    label: 'CONAN.conditions.gua',
    title: 'CONAN.conditionDescriptionGuar',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/hindered.png',
    id: 'hindered',
    label: 'CONAN.conditions.hin',
    title: 'CONAN.conditionDescriptionHind',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/poisoned.png',
    id: 'poisoned',
    label: 'CONAN.conditions.poi',
    title: 'CONAN.conditionDescriptionPois',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/prone.png',
    id: 'prone',
    label: 'CONAN.conditions.pro',
    title: 'CONAN.conditionDescriptionPron',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
  {
    icon: 'systems/conan2d20/assets/icons/conditions/staggered.png',
    id: 'staggered',
    label: 'CONAN.conditions.sta',
    title: 'CONAN.conditionDescriptionStag',
    flags: {
      conan2d20: {
        trigger: 'endRound',
        value: null,
      },
    },
  },
];

CONFIG.talentRanks = {
  1: 1,
  2: 2,
  3: 3,
};

CONFIG.transpoAnimals = {
  one: 'CONAN.transpoAnimals.1',
  onep: 'CONAN.transpoAnimals.1p',
  two: 'CONAN.transpoAnimals.2',
  twop: 'CONAN.transpoAnimals.2p',
  four: 'CONAN.transpoAnimals.4',
  fourp: 'CONAN.transpoAnimals.4p',
};

CONFIG.transpoBoatTypes = {
  bireme: 'CONAN.transpoBoatTypes.bireme',
  canoe: 'CONAN.transpoBoatTypes.canoe',
  carrack: 'CONAN.transpoBoatTypes.carrack',
  cog: 'CONAN.transpoBoatTypes.cog',
  galley: 'CONAN.transpoBoatTypes.galley',
  gondola: 'CONAN.transpoBoatTypes.gondola',
  kayak: 'CONAN.transpoBoatTypes.kayak',
  longboat: 'CONAN.transpoBoatTypes.longboat',
  longship: 'CONAN.transpoBoatTypes.longship',
  raft: 'CONAN.transpoBoatTypes.raft',
};

CONFIG.transpoCapabilities = {
  p: 'CONAN.transpoCapabilities.p',
  mp: 'CONAN.transpoCapabilities.mp',
  bmp: 'CONAN.transpoCapabilities.bmp',
};

CONFIG.transpoCartTypes = {
  carriage: 'CONAN.transpoCartTypes.carriage',
  cart: 'CONAN.transpoCartTypes.cart',
  hchar: 'CONAN.transpoCartTypes.hchar',
  lchar: 'CONAN.transpoCartTypes.lchar',
  litter: 'CONAN.transpoCartTypes.litter',
  wagon: 'CONAN.transpoCartTypes.wagon',
  pwagon: 'CONAN.transpoCartTypes.pwagon',
};

CONFIG.transpoCategories = {
  mounts: 'CONAN.transpoCategories.mounts',
  carts: 'CONAN.transpoCategories.carts',
  boats: 'CONAN.transpoCategories.boats',
};

CONFIG.transpoMountTypes = {
  buffalo: 'CONAN.transpoMountTypes.buffalo',
  camel: 'CONAN.transpoMountTypes.camel',
  donkey: 'CONAN.transpoMountTypes.donkey',
  dhorse: 'CONAN.transpoMountTypes.dhorse',
  rhorse: 'CONAN.transpoMountTypes.rhorse',
  whorse: 'CONAN.transpoMountTypes.whorse',
};

CONFIG.talentTypes = {
  homeland: 'CONAN.talentTypes.homeland',
  caste: 'CONAN.talentTypes.caste',
  bloodline: 'CONAN.talentTypes.bloodline',
  education: 'CONAN.talentTypes.education',
  nature: 'CONAN.talentTypes.nature',
  archetype: 'CONAN.talentTypes.archetype',
  skill: 'CONAN.talentTypes.skill',
  other: 'CONAN.talentTypes.other',
};

CONFIG.weaponQualities = {
  area: 'CONAN.qualities.weapons.area',
  backlashx: 'CONAN.qualities.weapons.back',
  blessedx: 'CONAN.qualities.weapons.bles',
  blinding: 'CONAN.qualities.weapons.blin',
  brilliant: 'CONAN.qualities.weapons.bril',
  cavalryx: 'CONAN.qualities.weapons.cava',
  cursedx: 'CONAN.qualities.weapons.curs',
  enchantedx: 'CONAN.qualities.weapons.ench',
  ensorcelledx: 'CONAN.qualities.weapons.enso',
  familiar: 'CONAN.qualities.weapons.fami',
  fearsomex: 'CONAN.qualities.weapons.fear',
  fragile: 'CONAN.qualities.weapons.frag',
  grappling: 'CONAN.qualities.weapons.grap',
  hiddenx: 'CONAN.qualities.weapons.hidd',
  improvised: 'CONAN.qualities.weapons.impr',
  intriguingx: 'CONAN.qualities.weapons.intr',
  incendiaryx: 'CONAN.qualities.weapons.ince',
  intense: 'CONAN.qualities.weapons.inte',
  keen: 'CONAN.qualities.weapons.keen',
  knockdown: 'CONAN.qualities.weapons.knoc',
  maledictionx: 'CONAN.qualities.weapons.male',
  nonlethal: 'CONAN.qualities.weapons.nonl',
  pairedx: 'CONAN.qualities.weapons.pair',
  parrying: 'CONAN.qualities.weapons.parr',
  patron: 'CONAN.qualities.weapons.patr',
  persistentx: 'CONAN.qualities.weapons.pers',
  piercingx: 'CONAN.qualities.weapons.pier',
  purposex: 'CONAN.qualities.weapons.purp',
  regalx: 'CONAN.qualities.weapons.rega',
  sanguinex: 'CONAN.qualities.weapons.sang',
  shieldx: 'CONAN.qualities.weapons.shie',
  spreadx: 'CONAN.qualities.weapons.spre',
  stun: 'CONAN.qualities.weapons.stun',
  subtlex: 'CONAN.qualities.weapons.subt',
  thrown: 'CONAN.qualities.weapons.thro',
  trappedx: 'CONAN.qualities.weapons.trap',
  unforgivingx: 'CONAN.qualities.weapons.unfo',
  viciousx: 'CONAN.qualities.weapons.vici',
  volley: 'CONAN.qualities.weapons.voll',
  weak: 'CONAN.qualities.weapons.weak',
};

CONFIG.weaponGroups = {
  axe: 'CONAN.weaponGroup.axe',
  bow: 'CONAN.weaponGroup.bow',
  club: 'CONAN.weaponGroup.clu',
  crossbow: 'CONAN.weaponGroup.cro',
  dagger: 'CONAN.weaponGroup.dag',
  flail: 'CONAN.weaponGroup.fla',
  flexible: 'CONAN.weaponGroup.fle',
  hidden: 'CONAN.weaponGroup.hid',
  improvised: 'CONAN.weaponGroup.imp',
  polearm: 'CONAN.weaponGroup.pol',
  shield: 'CONAN.weaponGroup.shi',
  sling: 'CONAN.weaponGroup.sli',
  sword: 'CONAN.weaponGroup.swo',
  spear: 'CONAN.weaponGroup.spe',
};

CONFIG.weaponTypes = {
  melee: 'CONAN.weaponTypes.melee',
  ranged: 'CONAN.weaponTypes.ranged',
};

CONFIG.weaponSizes = {
  none: 'CONAN.weaponSizes.no',
  oneHanded: 'CONAN.weaponSizes.1h',
  twoHanded: 'CONAN.weaponSizes.2h',
  unbalanced: 'CONAN.weaponSizes.ub',
  unwieldy: 'CONAN.weaponSizes.uw',
  fixed: 'CONAN.weaponSizes.fi',
  monstrous: 'CONAN.weaponSizes.mo',
};

CONFIG.weaponReaches = {
  1: '1',
  2: '2',
  3: '3',
  4: '4',
};

CONFIG.weaponRanges = {
  close: 'CONAN.weaponRanges.c',
  medium: 'CONAN.weaponRanges.m',
  long: 'CONAN.weaponRanges.l',
};

CONFIG.displayDamageDice = {
  x: 'X',
};

CONFIG.damageDice = {
  0: '0',
  1: '1',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
  6: '6',
  7: '7',
  8: '8',
  9: '9',
  10: '10',
  11: '11',
  12: '12',
  13: '13',
  14: '14',
  15: '15',
  16: '16',
  17: '17',
  18: '18',
  19: '19',
  20: '20',
};

CONFIG.soakDice = {
  light: '2dp',
  heavy: '4dp',
};

CONFIG.soakValue = {
  light: 'CONAN.SoakLight',
  heavy: 'CONAN.SoakHeavy',
};

CONFIG.weaponDescriptions = {
  axe: 'CONAN.weaponDescriptionAxe',
  bow: 'CONAN.weaponDescriptionBow',
  club: 'CONAN.weaponDescriptionClu',
  crossbow: 'CONAN.weaponDescriptionsCro',
  dagger: 'CONAN.weaponDescritionDag',
  dirk: 'CONAN.weaponDescriptionDir',
  flail: 'CONAN.weaponDescriptionFla',
  flexile: 'CONAN.weaponDescriptionFle',
  hammer: 'CONAN.weaponDescriptionHam',
  improvised: 'CONAN.weaponDescriptionImp',
  pick: 'CONAN.weaponDescriptionPic',
  polearm: 'CONAN.weaponDescriptionPol',
  shield: 'CONAN.weaponGroup.shi',
  sling: 'CONAN.weaponDescriptionSli',
  sword: 'CONAN.weaponDescriptionSwo',
  spear: 'CONAN.weaponDescriptionSpe',
};

CONFIG.qualitiesDescriptions = {
  heavy: 'CONAN.qualities.description.heav',
  noisy: 'CONAN.qualities.description.nois',
  veryheavy: 'CONAN.qualities.description.vhea',
  armorFragile: 'CONAN.qualities.description.afra',
  area: 'CONAN.qualities.description.area',
  backlashx: 'CONAN.qualities.description.back',
  blinding: 'CONAN.qualities.description.blin',
  cavalryx: 'CONAN.qualities.description.cava',
  fearsomex: 'CONAN.qualities.description.fear',
  fragile: 'CONAN.qualities.description.frag',
  grappling: 'CONAN.qualities.description.grap',
  hiddenx: 'CONAN.qualities.description.hidd',
  improvised: 'CONAN.qualities.description.impr',
  incendiaryx: 'CONAN.qualities.description.ince',
  intense: 'CONAN.qualities.description.inte',
  knockdown: 'CONAN.qualities.description.knoc',
  nonlethal: 'CONAN.qualities.description.nonl',
  paired: 'CONAN.qualities.description.pair',
  parrying: 'CONAN.qualities.description.parr',
  persistentx: 'CONAN.qualities.description.pers',
  piercingx: 'CONAN.qualities.description.pier',
  shieldx: 'CONAN.qualities.description.shie',
  spreadx: 'CONAN.qualities.description.spre',
  stun: 'CONAN.qualities.description.stun',
  subtlex: 'CONAN.qualities.description.subt',
  thrown: 'CONAN.qualities.description.thro',
  unforgivingx: 'CONAN.qualities.description.unfo',
  viciousx: 'CONAN.qualities.description.vici',
  volley: 'CONAN.qualities.description.voll',
  blessedx: 'CONAN.qualities.description.bles',
  brilliant: 'CONAN.qualities.description.bril',
  cursedx: 'CONAN.qualities.description.curs',
  enchantedx: 'CONAN.qualities.description.ench',
  ensorcelledx: 'CONAN.qualities.description.enso',
  intriguingx: 'CONAN.qualities.description.intr',
  maledictionx: 'CONAN.qualities.description.male',
  purposex: 'CONAN.qualities.description.purp',
  regalx: 'CONAN.qualities.description.rega',
  sanguinex: 'CONAN.qualities.description.sang',
  trappedx: 'CONAN.qualities.description.trap',
  patron: 'CONAN.qualities.description.patr',
  familiar: 'CONAN.qualities.description.fami',
  pairedx: 'CONAN.qualities.description.pair',
  brittle: 'CONAN.qualities.description.brit',
  bulky: 'CONAN.qualities.description.bulk',
  cool: 'CONAN.qualities.description.cool',
  courage: 'CONAN.qualities.description.cour',
  mentalPiercingx: 'CONAN.qualities.description.apie',
  warm: 'CONAN.qualities.description.warm',
  weak: 'CONAN.qualities.description.weak',
  keen: 'CONAN.qualities.description.keen',
  intimidating: 'CONAN.qualities.description.inti',
};
